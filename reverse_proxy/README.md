# Docker commands
My infra was using a stack of two containers to automatically generate ssl certificates and provide them to a Nginx reverse-proxy. The use of this stack allowed my containers to be accessible on the internet without having to expose diffrent ports for each services and having a lil bit of security and encryption.

I'm not an expert in security and SSL so I won't be able to properly tell the advantages nor disadvantages of free solutions like Let's encrypt, but you could easily find informations about that online.

The images I used for this setup is now deprecated and not maintained, so I had to switch to other images that basically do the same stuff with the same features.

I'm used to compose files when it comes to deploying stacks like that, but for testing purposes I had to use the docker commands to deploy something that is trully working. I had problems with the networks set in my compose files. I troobleshooted that and it is now working !

## Nginx reverse-proxy
This container will be used to redirect incoming requests to the right service/container using hostnames. Here's a diagram of what it does exactly : 

![diagram of the reverse proxy](./nginx_reverseproxy.png "reverse proxy diagram")

Here's the command to set this container up :
```
docker run --detach \
  --name nginx-proxy \
  --publish 80:80 \
  --publish 443:443 \
  --volume certs:/etc/nginx/certs \
  --volume vhost:/etc/nginx/vhost.d \
  --volume html:/usr/share/nginx/html \
  --volume /var/run/docker.sock:/tmp/docker.sock:ro \
  --network net2 \
nginxproxy/nginx-proxy
```

The certs, vhost and html volumes aren't necessary and will be used by our acme companion. The fourht volume, however, is essencial for our container to properly do it's job. It is mounted as a read-only (...:ro) volume to ensure that the container won't change anything in our docker.sock.

To indicate which container is to be proxied you only need to add an environment variable when running the said containers. This vairalbe looks like this :
```shell
VIRTUAL_HOST
```
And contains the string of the domain/subdomain given to you service.

**Exemple :**
```
docker run --detach \
  --name wordpress
  --network net2
  --env "VIRTUAL_HOST=wp.sheepo.fr"
wordpress:latest
```

## Acme companion
This container will generate SSL certificates for each service proxied by our nginx reverse-proxy. It will use some volumes to stock certificates and pass them to the Nginx container. The container uses Let's encrypt to generate the certificates. Let's update our diagram :
![diagram of the reverse proxy with the let's encrypt companion](./nginx_reverseproxy_acme.png "reverse proxy with the let's encrypt companion diagram")

This may not be the most readable diagram but it is the best I could come up with to summup how everything works.

Here's the command to set this container up :
```
docker run --detach \
  --name nginx-proxy-acme \
  --volumes-from nginx-proxy \
  --volume /var/run/docker.sock:/var/run/docker.sock:ro \
  --volume acme:/etc/acme.sh \
  --env "DEFAULT_EMAIL=urmailaddress@urdomain.com" \
  --network net2 \
nginxproxy/acme-companion
```
This container re-uses the volumes of the previously created nginx-proxy container. Both of them will share data thanks to these volumes : 
- the "certs" volume will be used to store certificates
- the "vhost" volume will be used to share the configuration of the vhosts
- the "html" volume will be used to store http-01 challenge files

One more volume (apart from the docker.sock volume) is added to store acme.sh configuration.

To indicate that the service/container need to contact the Let's encrypt companion we just use an environment variable like in this example :
**Example :**
```
docker run --detach \
  --name wordpress
  --network net2
  --env "VIRTUAL_HOST=wp.sheepo.fr
  --env "LETSENCRYPT_HOST=wp.sheepo.fr
wordpress:latest
```

The "LESTENCRYPT_HOST" variable indicates to Let's encrypt which subdommains must e used when generating the certificates for the containers.
# Docker-mailserver (DMS)
This compose file is a packaged mailserver running on a single docker container. To have the full explanation of the project [you can go there](https://docker-mailserver.github.io/docker-mailserver/latest/)

I'm going to write what I did to make my own mail server work, but note that for a resaon I ignore my mails are sent to spams. I still have to figure why.

## Reading docs
Not the funniest part of it all, but reading through the whole documentation is really helpfull to trully understand what this whole container is about.

[Here are the documentation you will have to read](https://docker-mailserver.github.io/docker-mailserver/latest/introduction/) to understand what we'll set up. If you are already familiar with mailservers and all those mechanisms just skip to the next section, even tho I don't recommend that, mostly because you will be given useful informations to work on the services themselves.

## Starting the installation
Every steps of the installation are [described here](https://docker-mailserver.github.io/docker-mailserver/latest/usage/)

### Minimal DNS setup

1. set an MX record for your domain = [domain_name] IN MX 1 [prefix].[domain_name]
2. set a A record = [prefix].[domain_name] IN A [servers_IP]
3. set a PTR = reverse DNS for your domain name

You can see the examples given in that part on the documentation given above

### Deploying the actual image
The most difficult part.

Once you've got the compose.yaml and mailserver.env files (or took the ones iven on this repo) you'll have to edit them so they correspond to what you want.

In the compose.yaml just find the "mail.example.com" and replace it with the subdomain created above in the A record.

The mailserver.env has a hell lot of options, but most of them are related to options you don't really want to use. Personnaly I just used the classic file and edited a few things that I'll put bellow. Just keep in mind that this image provides everything you'll need, you just need to find the proper options in the .env file.

**What I changed :**
- POSTMASTER_ADDRESS : the address of the postmaster
- TZ :  the timezone
- SPAM_SUBJECT : what to add in the subject of spam mails
- SSL_TYPE : what type of ssl is used

Note that we could leave SSL_TYPE empty to disable SSL, but most of the clients just wont connect to your server if you don't set SSL.

Next just follow the official documentation.

### Setting up TLS
As explained above TLS is a must have if you want to connect to any mail client. Just follow [this doc](https://docker-mailserver.github.io/docker-mailserver/latest/config/security/ssl/) to set up TLS for your container.

Note that you only need a certificate for your MX sub-domain. Those will be the ones the servers will check when trying to connect to your website. You can basically have any domain or sub-domain in the emails created, if the receiveing or sending (or both) server are using the MX sub-domainas mail servers you won't need any other certificate.

### Setting up DMARC, DKIM and SPF
If you try to send mails now you'll be bloqued by almost every servers, because SPF and DKIM are more and more used. So we'll set those things up, along with DMARC.
To do this [follow this guide](https://docker-mailserver.github.io/docker-mailserver/latest/config/best-practices/dkim_dmarc_spf/)

## Connecting to your mailbox
You can now connect to you mailbox with your favorite mail client and use it as it is (with the logins and passwords you created when you started the container)
